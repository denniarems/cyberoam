# Description
Cyberoam client for linux system (For debian & ubuntu derivatives)
A lazy attempt, code is not optimized, will update in future.
Just tired of doing the same thing repeatedly going to create a shell script to do the functionality.

# Installation

Before the installation you should know your 
cyberoam username and password as it will be prompted.

Also find your ethernet interface.

- You can find your ethernet interface by commands
- `ifconfig` or `ip addr`
The ethernet inteface will be something like `eth0` or `enp2s0`


Installation procedure

- Open a terminal 
execute following commands.

- `cd /tmp/`

- `git clone https://gitlab.com/shankar689/cyberoam.git`

- `cd cyberoam`

- `sudo chmod a+x install.sh`

- `sudo ./install.sh`

That will do the trick.

# Post Installation

- Just type in cyberoam in a terminal to loggin and cyberoamoff to logout

- Addtionally you could add this to start up to loggin on start up

- You will be ask to enter the Cyberoam IP, at spericorn the server is `192.168.1.1`