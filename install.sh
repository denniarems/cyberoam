#/bin/bash
apt-get install --reinstall libc6-i386 libgcc1-dbg gcc-multilib
cd ./data/
unzip cyberoamclient.zip && rm cyberoamclient.zip
cp -rvf cyberoamclient /opt/
read -p "Enter Your cyberoam user name: "  username

echo "Just type in ifconfig or ip addr on a new console"
echo "To know your Ethernet Network interface"
echo "It will be something like eth0 or enp2s0"
echo ""

read -p "Enter the Ethernet Network interface:" eth

echo "/opt/cyberoamclient/crclient -u $username -i $eth" > /bin/cyberoam
echo "/opt/cyberoamclient/crclient -l $username" > /bin/cyberoamoff

chmod a+x /bin/cyberoam
chmod a+x /bin/cyberoamoff

echo "Finished installation"
echo "Just type in cyberoam to loggin and cyberoamoff to logout"
echo "Addtionally you could add this to start up to loggin on start up"
echo ""